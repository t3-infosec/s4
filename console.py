import ssss as sh

k = input("Please enter k: ")
prime_choice = input("Please enter either 1 to use the 12th Mersenne Prime or 2 to use the 13th Mersenne Prime: ")
prime = -1
if prime_choice == '1':
    prime = 2 ** 127 - 1
elif prime_choice == '2':
    prime = 2 ** 521 - 1

sh.IntegerModP._prime = prime

points = []
x_s = []
for entry in range(1, int(k) + 1):
    x = int(input("Enter x (" + str(entry) + "/" + str((int(k))) + "): "))
    y = int(input("Enter y (" + str(entry) + "/" + str((int(k))) + "): "))
    x_s.append(x)
    points.append((x, y))

num_extra_shares = int(input("\nEnter the desired number of extra shares: "))
starting_x = int(input("Enter the starting x-value for the extra shares: "))

poly = sh.Polynomial.get_polynomial(*points)
print("\nPOLYNOMIAL")
print(poly)

print('\nEXTRA SHARES')
current_share_num = 0
i = 0
while current_share_num < num_extra_shares:
    if(starting_x + i) not in x_s:
        print("(" + str(starting_x + i) + ", " + str(poly.calculate(starting_x + i)) + ")")
        current_share_num += 1;
    i += 1

