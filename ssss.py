# THIS PROGRAM IMPLEMENTS A VERSION OF THE SHAMIR'S SECRET SHARING SCHEME
# WITH RANDOM INTEGER MODP POINTS AS THE SHARES AND THE RESULTING POLYNOMIAL, THROUGH LAGRANGE'S INTERPOLATION,
# AS THE SECRET.

# Class that allows the manipulation of the coefficient arrays of int modP polynomials
class Polynomial():
    def __init__(self, *coefficients):
        self.coefficients = []
        for coefficient in coefficients:
            self.coefficients.append(IntegerModP(coefficient)) # reduces the coefficient to its modP form, if needed

    def __str__(self):
        '''The string form of polynomials is written from the lowest degree to the highest.'''
        string = ""
        current_degree = 0
        for coefficient in self.coefficients:
            string += str(coefficient.value)
            string += "x^" + str(current_degree)
            if current_degree < len(self) - 1:
                string += "+\n"
            current_degree += 1
        string += "\nmod " + str(IntegerModP._prime)
        return str(string)

    def __add__(self, other):
        longest = self
        shortest = other
        if len(self) < len(other):
            longest = other
            shortest = self
        result = longest
        for i in range(len(shortest)):
            result.coefficients[i] += shortest.coefficients[i]
        return result

    def __mul__(self, other):
        result = Polynomial()
        if type(other) is int:
            size = len(self)
            for e in range(size):
                result.coefficients.append(IntegerModP(0))
            for i in range(len(self.coefficients)):
                result.coefficients[i].value = self.coefficients[i].value * other % IntegerModP._prime
            return result
        size = len(self) + len(other) - 1
        for e in range(size):
            result.coefficients.append(IntegerModP(0))
        for i in range(len(self)):
            for j in range(len(other)):
                result.coefficients[i + j] = (result.coefficients[i + j] + self.coefficients[i] * other.coefficients[j]) % IntegerModP._prime
        return result

    def __len__(self):
        return len(self.coefficients)

    def calculate(self, x):
        y = 0
        for i in range(len(self.coefficients)):
            y += x ** i * self.coefficients[i].value
        return y % IntegerModP._prime

    def divide(self, denominator):
        for i in range(len(self.coefficients)):
            self.coefficients[i] = self.coefficients[i].divide(denominator)
        return self

    def get_polynomial(*points):
        sum = Polynomial()
        for point in points:
            numerator = Polynomial(1)
            denominator = 1
            for other in points:
                if other != point:
                    numerator *= Polynomial(-other[0], 1)
                    denominator *= Polynomial(-other[0], 1).calculate(point[0])
            sum += numerator.divide(denominator) * point[1]
        return sum

class IntegerModP():
    _prime = 2 ** 521 - 1

    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return self.value

    def __str__(self):
        return str(self.value)

    def __add__(self, other):
        result = IntegerModP(0)
        result.value = self.value + other.value
        result.value = result.value % IntegerModP._prime
        return result

    def __mul__(self, other):
        result = IntegerModP(0)
        result.value = (self.value * other.value) % IntegerModP._prime
        return result

    def __pow__(self, power, modulo=None):
        self.value = self.value ** power % IntegerModP._prime
        return self

    def __mod__(self, other):
        self.value = self.value % IntegerModP._prime
        return self

    @staticmethod
    def modInverse(a, m):

        g = IntegerModP.gcd(a, m)

        if (g != 1):
            print("Inverse doesn't exist")

        else:
            # If a and m are relatively prime,
            # then modulo inverse is a^(m-2) mode m
            m0 = m
            y = 0
            x = 1

            if (m == 1):
                return 0

            while (a > 1):
                # q is quotient
                q = a // m

                t = m

                # m is remainder now, process
                # same as Euclid's algo
                m = a % m
                a = t
                t = y

                # Update x and y
                y = x - q * y
                x = t

            # Make x positive
            if (x < 0):
                x = x + m0

            return IntegerModP(x)

    @staticmethod
    def gcd(a, b):
        if a == 0:
            return b
        return IntegerModP.gcd(b % a, a)

    def divide(self, other):
        modInverse = IntegerModP.modInverse(other, IntegerModP._prime)
        return self * modInverse % IntegerModP._prime

