import tkinter as tk
from tkinter import messagebox
from argon2 import PasswordHasher

import os.path
import ssss as sh
import scrollable as scrollable
import primality as primality

class generate_secret():
    def __init__(self, root):
        self.root = root
        self.left_frame = tk.Frame(self.root)
        self.left_frame.grid(row=1, column=1)
        self.right_frame = tk.Frame(self.root)
        self.right_frame.grid(row=1, column=2)
        self.k = 0
        self.number_of_extra_shares = 0
        self.polynomial_frame = tk.Frame(self.right_frame)
        self.shares_entries_frame = tk.Frame(self.left_frame)
        self.extra_shares_frame = tk.Frame(self.right_frame)
        self.scrollable_frame = scrollable.ScrollableFrame(self.shares_entries_frame)
        self.popup_frame = tk.Frame(self.root)
        self.initiate_display()

    def initiate_display(self):
        initial_info_frame = tk.Frame(self.left_frame)
        initial_info_frame.pack()

        number_of_shares_label = tk.Label(initial_info_frame, text="k:")
        number_of_shares_label.grid(row=1, column=1)

        self.number_of_shares_entry = tk.Entry(initial_info_frame, width=30)
        self.number_of_shares_entry.grid(row=1, column=2, sticky='w', columnspan=2)

        prime_label = tk.Label(initial_info_frame, text="Prime:")
        prime_label.grid(row=2, column=1, sticky='n')

        # prime radiobuttons
        options_frame = tk.Frame(initial_info_frame)
        options_frame.grid(row=2, column=2)
        self.selected = tk.IntVar(value=1)
        twelfth_mersenne = tk.Radiobutton(options_frame, text='12th Mersenne prime', value=1,
                                          variable=self.selected, command=self.prime_callback)
        twelfth_mersenne.grid(row = 1, sticky='w', columnspan=2)
        thirteenth_mersenne = tk.Radiobutton(options_frame, text='13th Mersenne prime', value=2,
                                             variable=self.selected, command=self.prime_callback)
        thirteenth_mersenne.grid(row = 2, sticky='w', columnspan=2)
        other = tk.Radiobutton(options_frame, text='Other: ', value=3, variable=self.selected, command=self.prime_callback)
        other.grid(row = 3, sticky='w', columnspan=2)
        # field for 'other' option
        self.prime_entry = tk.Entry(options_frame, state='disabled')
        self.prime_entry.grid(row=4, column=1)

        self.submit_initial_info = tk.Button(options_frame, text="Submit", command=self.get_initial_info, width=5, state='disabled')
        self.submit_initial_info.grid(row=4, column=2, columnspan=1)

    def prime_callback(self):
        '''Manages the state of the radio buttons and the "other" entry field as different options are selected'''
        if self.selected.get() == 3:
            self.prime_entry.config(state='normal')
            self.submit_initial_info.config(state='normal')
        else:
            self.prime_entry.config(state='disabled')
            self.submit_initial_info.config(state='disabled')
            self.get_initial_info()

    def is_prime(self, num):
        if primality.isPrime(num, 7):
            return True
        return False

    def get_initial_info(self):
        if self.number_of_shares_entry.get() == '':
            return messagebox.showinfo("Missing number of shares",
                                       "Please enter how many shares you wish to input.")
        if not self.number_of_shares_entry.get().isdigit():
            return messagebox.showinfo("Not a number",
                                       "Please enter a number in the k field to proceed.")
        elif int(self.number_of_shares_entry.get()) < 3:
            return messagebox.showinfo("Too few shares", "Please enter at least 3 shares.")
        self.k = int(self.number_of_shares_entry.get())

        # assigns prime according to the chosen option
        if self.selected.get() == 1:
            sh.IntegerModP._prime = 2 ** 127 - 1
        elif self.selected.get() == 2:
            sh.IntegerModP._prime = 2 ** 521 - 1
        # checks if custom prime is valid
        elif self.selected.get() == 3:
            if self.prime_entry.get() == '':
                return messagebox.showinfo("Missing prime", "Please enter a prime number to proceed.")
            elif not self.prime_entry.get().isdigit():
                return messagebox.showinfo("Not a number", "Please enter a number in the prime field to proceed.")
            elif int(self.prime_entry.get()) < 2:
                return messagebox.showinfo("Unapplicable prime", "Please enter a prime number higher than 2.")
            elif not self.is_prime(int(self.prime_entry.get())):
                return messagebox.showinfo("Not a prime", "Please enter a prime number.")
            sh.IntegerModP._prime = int(self.prime_entry.get())
        self.display_share_entries()

    def display_share_entries(self):
        # erase widgets
        for widget in self.shares_entries_frame.winfo_children():
            widget.grid_forget()
        for widget in self.scrollable_frame.winfo_children():
            widget.grid_forget()
        self.shares_entries_frame.pack()

        # entries grid
        x_label = tk.Label(self.scrollable_frame, text="x")
        x_label.grid(row=1, column=1)
        y_label = tk.Label(self.scrollable_frame, text="y")
        y_label.grid(row=1, column=2)
        self.entries_x_list = []
        self.entries_y_list = []
        for i in range(self.k):
            self.entries_x_list.append(tk.Entry(self.scrollable_frame))
            self.entries_x_list[i].grid(row=i + 2, column=1)
            self.entries_y_list.append(tk.Entry(self.scrollable_frame))
            self.entries_y_list[i].grid(row=i + 2, column=2)

        # extra shares
        extra_shares_label = tk.Label(self.scrollable_frame, text="Number of extra shares: ")
        extra_shares_label.grid(row=self.k + 2, column=1)
        self.number_of_extra_shares_entry = tk.Entry(self.scrollable_frame)
        self.number_of_extra_shares_entry.grid(row=self.k + 2, column=2)

        starting_x_for_shares_label = tk.Label(self.scrollable_frame, text="Starting x for shares: ")
        starting_x_for_shares_label.grid(row=self.k + 3, column=1)
        self.starting_x_for_shares_entry = tk.Entry(self.scrollable_frame)
        self.starting_x_for_shares_entry.grid(row=self.k + 3, column=2)

        submit_button = tk.Button(self.scrollable_frame, text="Submit", command=self.get_points)
        submit_button.grid(row=self.k + 4, columnspan=3)
        self.scrollable_frame.update()

    def copy(self, string):
        self.root.clipboard_clear()
        self.root.clipboard_append(string)

    def show_popup(self):
        if self.popup_frame.winfo_children():
            return
        popup = tk.Toplevel(self.popup_frame)
        popup.title = "Hash parameters"
        memory_cost_label = tk.Label(popup, text="Memory cost (kibibytes):")
        memory_cost_label.grid(row=1, column=1)
        memory_cost_input = tk.Entry(popup)
        memory_cost_input.grid(row=1, column=2)
        iterations_label = tk.Label(popup, text="Number of iterations:")
        iterations_label.grid(row=1, column=3)
        iterations_input = tk.Entry(popup)
        iterations_input.grid(row=1, column=4)
        parallelism_label = tk.Label(popup, text="Number of parallel threads:")
        parallelism_label.grid(row=2, column=1)
        parallelism_input = tk.Entry(popup)
        parallelism_input.grid(row=2, column=2)


    def hash_argon(self, poly):
        ph = PasswordHasher()
        hashed = ph.hash(str(poly)) + "\n"
        self.copy(hashed)
        self.update_field(self.polynomial_label, hashed)
        return messagebox.showinfo("Hash ready", "The hash is on your clipboard")

    def update_field(self, field, new_text):
        field.insert(1.0, new_text)

    def get_points(self):
        for widget in self.right_frame.winfo_children():
            widget.pack_forget()
        self.point_xs = []
        def save_to_file():
            if os.path.exists("reconstructed_polynomial.txt"):
                answer = messagebox.askyesno("File already exists", "A file with the reconstructed polynomial already exists; proceeding will overwrite it. Proceed?")
                if not answer:
                    return
            with open("reconstructed_polynomial.txt", "w") as file:
                file.write("Prime: " + str(sh.IntegerModP._prime) + "\n\n" + str(self.poly))

        for widget in self.polynomial_frame.winfo_children():
            widget.pack_forget()
        if self.number_of_extra_shares_entry.get().isspace() or self.number_of_extra_shares_entry.get().isspace() == "" or self.number_of_extra_shares_entry.get() == "0":
            self.number_of_extra_shares = 0
        elif not self.number_of_extra_shares_entry.get().isdigit():
            return messagebox.showinfo("Not a number", "Please enter a number in the 'extra shares' field to proceed.")
        else:
            self.number_of_extra_shares = int(self.number_of_extra_shares_entry.get())
        self.points = []

        for i in range(self.k):
            if self.entries_x_list[i].get() == '' or self.entries_y_list[i].get() == '':
                return messagebox.showinfo("Empty coordinates", "Please fill all empty fields before submitting.")
            if not self.entries_x_list[i].get().isdigit() or not self.entries_y_list[i].get().isdigit():
                return messagebox.showinfo("Not a number", "Please enter only numbers in the coordinate fields to proceed.")
            self.points.append((int(self.entries_x_list[i].get()), int(self.entries_y_list[i].get())))
            self.point_xs.append((int(self.entries_x_list[i].get())))
        if len(self.point_xs) != len(set(self.point_xs)):
            return messagebox.showinfo("Repeated x-coordinates", "Please enter unique x-coordinates.")

        vertical_scroll = tk.Scrollbar(self.polynomial_frame, orient="vertical")
        horizontal_scroll = tk.Scrollbar(self.polynomial_frame, orient="horizontal")
        self.poly = sh.Polynomial.get_polynomial(*self.points)
        self.polynomial_label = tk.Text(self.polynomial_frame, yscrollcommand=vertical_scroll, xscrollcommand=horizontal_scroll, height=7, wrap=tk.NONE)
        vertical_scroll.config(command=self.polynomial_label.yview)
        horizontal_scroll.config(command=self.polynomial_label.xview)
        self.polynomial_label.insert(1.0, str(self.poly))
        vertical_scroll.pack(side="right", fill="y")
        horizontal_scroll.pack(side='bottom', fill='x')
        self.polynomial_label.pack(fill='x', expand=True)
        self.polynomial_frame.pack(fill='x', expand=True)
        button_frame = tk.Frame(self.right_frame)
        button_frame.pack()
        save_to_text_button = tk.Button(button_frame, text="Save", command=save_to_file)
        copy_button = tk.Button(button_frame, text="Paste", command=lambda: self.copy(self.poly))
        hash_button = tk.Button(button_frame, text="Hash", command=self.show_popup)
        save_to_text_button.pack(side='left')
        copy_button.pack(side='left')
        hash_button.pack(side='left')
        self.get_extra_shares()

    def get_extra_shares(self):
        def save_to_file():
            if os.path.exists("extra_shares.txt"):
                answer = messagebox.askyesno("File already exists", "A file with extra shares already exists; proceeding will overwrite it. Proceed?")
                if not answer:
                    return
            with open("extra_shares.txt", "w") as file:
                file.write("Prime: " + str(sh.IntegerModP._prime) + "\n\n" + self.extra_shares)

        def paste(string):
            self.root.clipboard_clear()
            self.root.clipboard_append(string)

        for widget in self.extra_shares_frame.winfo_children():
            widget.pack_forget()
        if self.number_of_extra_shares < 1:
            return
        vertical_scroll = tk.Scrollbar(self.extra_shares_frame, orient="vertical")
        horizontal_scroll = tk.Scrollbar(self.extra_shares_frame, orient="horizontal")
        self.shares_scrollable = tk.Text(self.extra_shares_frame, yscrollcommand=vertical_scroll, xscrollcommand=horizontal_scroll, wrap=tk.NONE)
        vertical_scroll.config(command=self.shares_scrollable.yview)
        horizontal_scroll.config(command=self.shares_scrollable.xview)
        self.extra_shares = """"""
        current_x = 0
        if self.starting_x_for_shares_entry.get().isspace():
            current_x = 0
        elif not self.starting_x_for_shares_entry.get().isdigit():
            return messagebox.showinfo("Not a number", "Please enter a number as the starting x to proceed.")
        else:
            current_x = int(self.starting_x_for_shares_entry.get())
        found_shares = 0
        while found_shares < self.number_of_extra_shares:
            if current_x in self.point_xs:
                current_x += 1
                continue
            if found_shares < self.number_of_extra_shares - 1:
                self.extra_shares += "(" + str(current_x) + ", " + str(self.poly.calculate(current_x)) + ")\n\n"
            else:
                self.extra_shares += "(" + str(current_x) + ", " + str(self.poly.calculate(current_x)) + ")"
            found_shares += 1
            current_x += 1
        self.shares_scrollable.insert(1.0, self.extra_shares)
        horizontal_scroll.pack(side='bottom', fill='x')
        vertical_scroll.pack(side="right", fill="y")
        self.extra_shares_frame.pack(fill='x', expand=True)
        self.shares_scrollable.pack(fill='x', expand=True)
        button_frame = tk.Frame(self.right_frame)
        button_frame.pack()
        save_to_text_button = tk.Button(button_frame, text="Save", command=save_to_file)
        copy_button = tk.Button(button_frame, text="Paste", command=lambda: paste(self.extra_shares))
        save_to_text_button.pack(side='left')
        copy_button.pack(side='left')

# Creates root window and runs the code
root = tk.Tk()
window_height = 650
window_width = 800

screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()

x_coordinate = int((screen_width/2) - (window_width/2))
y_coordinate = int((screen_height/2) - (window_height/2) - 40)

root.geometry("{}x{}+{}+{}".format(window_width, window_height, x_coordinate, y_coordinate))
display = generate_secret(root)
root.mainloop()